<?php
namespace Comprobo\Verify;

use PHPUnit\Framework\TestCase;

use Comprobo\Verify\State;
use Comprobo\Verify\Factory;
use Comprobo\Verify\Auth\Server as AuthServer;

class ApiTest extends TestCase
{
    private $state;
    private $comprobo;
    private $authServer;
    private $sut;

    public function setUp()
    {
        $this->state   = $this->createMock(State::class);
        $this->factory = $this->createMock(Factory::class);

        $this->authServer = $this->getMockBuilder(AuthServer::class)->disableOriginalConstructor()->getMock();
        $this->factory->expects($this->once())->method('getConfig')->will($this->returnValue(['env' => 'local']));
        $this->factory->expects($this->once())->method('getState')->will($this->returnValue($this->state));
        $this->factory->expects($this->once())->method('getServerAuth')->will($this->returnValue($this->authServer));

        $this->sut     = new Api($this->factory);
    }

    public function testApiSetsTokenDataInState()
    {
        $token = 'some brilliant token';
        $org   = 'pies';

        $this->state->expects($this->at(0))->method('set')->with('organisationId', $org);
        $this->state->expects($this->at(1))->method('set')->with('token', $token);

        $this->authServer->method('authenticate')->will($this->returnValue([ 'token' => $token, 'orgId' => $org ]));

        $result = $this->sut->authenticate('any', 'oldthing');

        $this->assertTrue($result, 'authentication with correct calls to state');
    }

    public function testApiDoesntKeepGarbageTokenDataInState()
    {
        $testKey    = 'my great key';
        $testSecret = 'my well gud secret';

        $this->authServer->method('authenticate')->with($testKey, $testSecret)->will($this->returnValue([
            'orgId' => 'pies'
        ]));

        $result = $this->sut->authenticate($testKey, $testSecret);

        $this->assertFalse($result, 'authentication fails because there is no token returned from the auth service');
    }
}
