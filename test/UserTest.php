<?php
namespace Comprobo\Verify;

use PHPUnit\Framework\TestCase;

use Comprobo\Verify\User;
use Comprobo\Verify\State;
use Comprobo\Verify\HTTP\Request;
use GuzzleHttp\Psr7\Response;

class UserTest extends TestCase
{
    private $sut;
    private $state;
    private $request;
    private $response;
    private $factory;

    public function setUp()
    {
        $this->factory  = $this->createMock(Factory::class);
        $this->request  = $this->createMock(Request::class);
        $this->response = $this->createMock(Response::class);
        $this->state    = new State; // using real state, might as well while it's in memory

        $this->state->set('token', 'some excellent token');
        $this->state->set('organisationId', 'some excellent org id');

        $this->factory->expects($this->once())->method('getConfig')->will($this->returnValue([
            'env' => 'local',
            'urls' => [
                'userCreate' => 'example.com/some/great/url'
            ]
        ]));
        $this->factory->method('getState')->will($this->returnValue($this->state));
        $this->factory->method('getRequest')->will($this->returnValue($this->request));

        $this->sut = new User($this->factory);
    }

    /**
     * @dataProvider publicMethods
     */
    public function testSanity($method, $desc)
    {
        $this->assertTrue(method_exists($this->sut, $method), $desc);
    }

    public function publicMethods()
    {
        return [
            'set a' => ['create', 'create method must exist']
        ];
    }

    public function testCreateIsCalledCorrectly()
    {
        $userId    = 'my custom user id';
        $firstName = 'blah blah';
        $lastName  = 'whatsit';

        $this->request->expects($this->once())->method('authorize')->will($this->returnValue(true));
        $this->request->expects($this->once())->method('__call')->will($this->returnValue($this->response));
        $this->response->method('getBody')->will($this->returnValue('{"somedata":"somedata"}'));

        $result = $this->sut->create($userId, $firstName, $lastName);
        $this->assertEquals(['somedata' => 'somedata'], $result, 'expected response from create');
    }
}
