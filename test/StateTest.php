<?php
namespace Comprobo\Verify;

use PHPUnit\Framework\TestCase;

use Comprobo\Verify\State;

class StateTest extends TestCase
{
    private $sut;

    public function setUp()
    {
        $this->sut = new State;
    }

    /**
     * @dataProvider publicMethods
     */
    public function testSanity($method, $desc)
    {
        $this->assertTrue(method_exists($this->sut, $method), $desc);
    }

    public function publicMethods()
    {
        return [
            'set a' => ['set', 'Set method must exist'],
            'set b' => ['get', 'Get method must exist'],
            'set c' => ['has', 'Has method must exist']
        ];
    }

    /**
     * @dataProvider setGetData
     */
    public function testSetGet($key, $value, $desc)
    {
        $this->sut->set($key, $value);
        $result = $this->sut->get($key);
        $this->assertEquals($value, $result, $desc);
    }

    public function setGetData()
    {
        return [
            'set a' => ['somekey', 'some data', 'retrieving string data'],
            'set b' => ['somekey', 1, 'retrieving int data'],
            'set c' => ['somekey', false, 'retrieving boolean data']
        ];
    }

    public function testSetHas()
    {
        $this->sut->set('existing', 'some data');

        $result = $this->sut->has('existing');
        $this->assertTrue($result, 'this key should exist');

        $result = $this->sut->has('not set!!!');
        $this->assertFalse($result, 'this key should not exist');
    }
}
