<?php
namespace Comprobo\Verify\HTTP;

/**
 * A thin wrapper around Guzzle with convenience methods for signing outgoing requests.
 *
 * Internal use only, subject to change
 */
class Request
{
    private $request;
    private $token;

    public function __construct()
    {
        $this->request = new \GuzzleHttp\Client(['timeout' => 20.0, 'connect_timeout' => 20.0, 'http_errors' => false]);
    }

    public function __call($method, $args)
    {
        $methodName = strtolower($method);
        if ($methodName === 'get' || $methodName === 'post' || $methodName === 'put' || $methodName === 'patch') {
            $options = isset($args[1]) ? $args[1] : [];
            $args[1] = $this->mungeOptions($options);
        }
        if ($methodName === 'request') {
            $options = isset($args[2]) ? $args[2] : [];
            $args[2] = $this->mungeOptions($options);
        }

        return call_user_func_array([$this->request, $method], $args);
    }

    /**
     * AmericaniZed spelling because why not.
     *
     * @param  string $token an access token for Comprobo systems
     * @return self
     */
    public function authorize($token)
    {
        $this->token = $token;
        return $this;
    }

    private function mungeOptions($options)
    {
        $options['headers'] = isset($options['headers']) ? $options['headers'] : [];
        if (!array_key_exists('Accept', $options['headers'])) {
            $options['headers']['Accept'] = 'application/json';
        }

        if ($this->token && !array_key_exists('Authorization', $options['headers'])) {
            $options['headers']['Authorization'] = 'Bearer ' . $this->token;
        }

        return $options;
    }
}
