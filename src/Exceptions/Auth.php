<?php
namespace Comprobo\Verify\Exceptions;

/**
 * Problems around authentication such as invalid credentials or server errors
 */
class Auth extends \Exception
{

}
