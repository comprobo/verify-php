<?php
namespace Comprobo\Verify\Exceptions;

/**
 * Throw when required configuration options are missing or invalid
 */
class Misconfiguration extends \Exception
{

}
