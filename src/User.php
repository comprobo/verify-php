<?php
namespace Comprobo\Verify;

use GuzzleHttp\Exception as GuzzleExceptions;

/**
 * User service integration
 */
class User
{
    private $config;
    private $factory;
    private $state;

    public function __construct(Factory $factory)
    {
        $this->factory = $factory;
        $this->config  = $factory->getConfig();
        $this->state   = $factory->getState();
    }

    /**
     * Create a new user in Comprobo. This user will not be given a direct login of their own,
     * but must be impersonated to be used.
     *
     * @see  Comprobo\Verify\Api::impersonate method to impersonate a user when we have their unique ID.
     *
     * @throws Exceptions\Auth if the state is invalid
     *
     * @param  string $userId    the local ID you would like to be associated with this user.
     * @param  string $firstName assigned to the user account and may be displayed in reports or other feedback.
     * @param  string $lastName  assigned to the user account and may be displayed in reports or other feedback.
     * @param  string $email     assigned to the user account and may be displayed in reports or other feedback.
     * @return array             the service response. Responsibilty falls on the caller to validate
     *                           and extract relevant information
     */
    public function create($userId, $firstName, $lastName, $email=null)
    {
        $this->validateState();

        $url = $this->config['urls']['userCreate'];
        $details = [
            'userId'     => $userId,
            'givenNames' => $firstName,
            'familyName' => $lastName,
        ];

        // email address is now optional
        if ($email) {
            $details['email'] = $email;
        }

        $request = $this->factory->getRequest();
        $request->authorize($this->state->get('token'));
        $response = $request->post($url, ['json' => $details]);
        $body = json_decode((string) $response->getBody(), true);

        return $body;
    }

    /**
     * Store an uploaded file against the given user to be used in monitoring as a reference photograph
     *
     * @param string $userId Your user id, as specified in the create call
     * @param array  $file   matching the $_FILES['my-file'] format, an array representing a file.
     * @throws Exceptions\Upload if the file is empty or an upload error has occurred.
     */
    public function setReferencePhoto($userId, array $file)
    {
        $this->validateState();
        $this->validateUpload($file);

        $url = str_replace('{userId}', $userId, $this->config['urls']['setRefPhoto']);

        $request = $this->factory->getRequest();
        $request->authorize($this->state->get('token'));
        $body = [
            'multipart' => [
                [
                    'name'     => 'fileContents',
                    'contents' => fopen($file['tmp_name'], 'r'),
                    'filename' => $file['name']
                ],
                [
                    'name'     => 'fileData',
                    'contents' => json_encode([
                        'type' => $file['type'],
                        'size' => $file['size']
                    ])
                ]
            ]
        ];

        try {
            $response = $request->post($url, $body);
        } catch(GuzzleExceptions $e) {
            $response = $e->getResponse();
        }

        $body = json_decode((string) $response->getBody(), true);

        return $body;
    }

    /**
     * Check to see if a reference photo exists for the specified User
     *
     * @param string $userId Your user id, as specified in the create call
     * @throws Exceptions\User if an unexpected error has occurred.
     */
    public function referencePhotoExists($userId)
    {
        $this->validateState();

        $url = str_replace('{userId}', $userId, $this->config['urls']['headRefPhoto']);

        $request = $this->factory->getRequest();
        $request->authorize($this->state->get('token'));

        try {
            $response = $request->head($url);
            if ($response->getStatusCode() === 200) {
                return true;
            } else if ($response->getStatusCode() === 404) {
                return false;
            }
        } catch(GuzzleExceptions $e) {
            $response = $e->getResponse();
        }

        $body = json_decode((string) $response->getBody(), true);

        throw new Exceptions\User($body != NULL && is_array($body) && array_key_exists('message', $body) 
            ? $body['message'] 
            : 'Internal Error');
    }

    private function validateState()
    {
        if (!$this->state->has('token')) {
            throw new Exceptions\Auth('Could not validate user request');
        }
    }

    private function validateUpload(array $file)
    {
        if (!isset($file) || empty($file)) {
            // no file uploaded top lol
            throw new Exceptions\Upload('No reference photo supplied');
        }

        if (isset($file['error']) && $file['error'] === \UPLOAD_ERR_OK) {
            return true;
        }

        $message = '';

        // something wrong with the uploaded file
        switch($file['error']) {
            case \UPLOAD_ERR_INI_SIZE:
                // The uploaded file exceeds the upload_max_filesize directive in php.ini.
                $max = ini_get('upload_max_filesize');
                $sent =  $file['size'];
                $message = 'Uploaded file is too large, maximum size is ' . $max;
                break;
            case \UPLOAD_ERR_PARTIAL:
                // 3; The uploaded file was only partially uploaded.
                $message = 'Incomplete file upload, please try again';
                break;
            case \UPLOAD_ERR_NO_FILE:
                // 4; No file was uploaded.
                $message = 'No file was uploaded';
                break;
            case \UPLOAD_ERR_NO_TMP_DIR:
                // temp folder missing, check upload_tmp_dir!
                // fall through
            case \UPLOAD_ERR_CANT_WRITE:
                // couldn't write the file to disk, is your disk full?
                // fall through
            case \UPLOAD_ERR_EXTENSION:
                // some extension broke, but we don't know which one
                $message = 'Could not save uploaded file';
                break;

            default:
                $message = "An unknown upload error occurred";
        }

        throw new Exceptions\Upload($message);
    }
}
