<?php
namespace Comprobo\Verify;

/**
 * Workflow service integration
 */
class Workflow
{
    private $config;
    private $factory;
    private $state;

    public function __construct(Factory $factory)
    {
        $this->factory = $factory;
        $this->config = $factory->getConfig();
        $this->state = $factory->getState();
    }

    /**
     * Assign a local ID and settings to the given template ready for use.
     *
     * @param  string $templateId    the chosen template ID (uuidv4)
     * @param  string $monitorableId an app-specific identifier to target this monitoring workflow in future
     * @param  string $friendyName   The name that will appear in Reporting and elsewhere, human readable
     * @param  array  $customFields  any merge fields defined by the template.
     * @return array                 Service response. It is the caller's responsibility to validate and extract info
     *
     * @see Comprobo\Verify\Workflow::listTemplates for how to find available templates
     * @see Comprobo\Verify\Workflow::getMergeFields for how to fetch available merge fields for a template
     */
    public function link($templateId, $monitorableId, $friendyName, array $customFields = [])
    {
        if (!$this->state->has('token')) {
            throw new Exceptions\Auth("Could not authenticate link template request");
        }

        $url = str_replace('{templateId}', $templateId, $this->config['urls']['linkTemplate']);

        $request = $this->factory->getRequest();
        $request->authorize($this->state->get('token'));

        $params = [
            'name'          => $friendyName . ' [' . time() . ']',
            'monitorableId' => $monitorableId
        ];

        if (!empty($customFields)) {
            $params['mergeValues'] = $customFields;
        }

        $response = $request->post($url, ['json' => $params]);
        $body     = json_decode((string) $response->getBody(), true);

        return $body;
    }

    /**
     * Request the parameters to give to the JavaScript SDK in order to
     * begin the monitoring for your monitorable activity.
     *
     * Once this has been called the configuration for the `link`ed template
     * identified by $monitorableId will be locked.
     *
     * @see self::link
     *
     * @param $userId your local user ID
     * @param $monitorableId your local monitorable ID provided during the `link` call
     */
    public function activate($userId, $monitorableId)
    {
        if (!$this->state->has('token')) {
            throw new Exceptions\Auth("Could not authenticate activate monitoring request");
        }

        $url = str_replace('{monitorable}', $monitorableId, $this->config['urls']['begin']);
        $url = str_replace('{userId}', $userId, $url);

        $request = $this->factory->getRequest();
        $request->authorize($this->state->get('token'));

        $response = $request->post($url, ['json' => ['userId' => $userId]]);
        $body     = json_decode((string) $response->getBody(), true);

        return $body;
    }

    /**
     * Fetch the list of available template types.
     *
     * @return array The list of results from the service
     */
    public function listTemplates()
    {
        if (!$this->state->has('token')) {
            throw new Exceptions\Auth("Could not authenticate list templates request");
        }

        $request = $this->factory->getRequest();
        $request->authorize($this->state->get('token'));
        $response = $request->get($this->config['urls']['templateList']);
        $body = json_decode((string) $response->getBody(), true);

        return $body;
    }
}
