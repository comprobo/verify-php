<?php
namespace Comprobo\Verify;

use Comprobo\Verify\Factory;

/**
 * Comprobo\Verify\Api is the public API for the Verify SDK. All public methods should be considered
 * normal operation of the SDK.
 *
 * It is possible to retrieve individual objects and make calls directly to different services although
 * this should not be a requirement for normal usage.
 *
 * USAGE:
 *
 * $verify = Comprobo\Verify\Api::create();
 * $verify->authenticate($key, $secret);
 *
 * ...
 *
 */
class Api
{
    private $factory;
    private $config;
    private $state;

    const DEFAULT_ENV = 'production';

    /**
     * Bootstrap the Verify API. Custom configuration options are currently not supported.
     *
     * @param string $env  'local', 'staging' or 'production'
     * @param array  $config additional configuration options
     * @return Comprobo\Verify\Api
     */
    public static function create($env = self::DEFAULT_ENV, array $config = [])
    {
        if (!is_string($env) || empty($env)) {
            $env = self::DEFAULT_ENV;
        }

        $config['env'] = $env;
        $factory = new Factory($config);
        return new static($factory);
    }

    /**
     * Used by the factory itself. If you happen to need to instantiate the API this way then you can.
     *
     * @param Factory $factory
     */
    public function __construct(Factory $factory)
    {
        $this->factory = $factory;
        $this->config  = $factory->getConfig();
        $this->state   = $factory->getState();
    }

    /**
     * Helper method to provide javascript asset location.
     *
     * @return string path to JavaScript file for inclusion where Verify is to be used
     */
    public function javascriptPath()
    {
        return $this->factory->javascriptPath();
    }

    /**
     * Helper method to provide css asset location.
     *
     * @return string path to css file for inclusion where Verify is to be used
     */
    public function cssPath()
    {
        return $this->factory->cssPath();
    }

    /**
     * Use your application key and secret
     * to set JWT token and organisation ID
     * in the current API context
     *
     * After this is called, you may continue to use the rest of the API
     * methods provided here.
     *
     * @param  $key    string
     * @param  $secret string
     * @throws Exceptions\Auth if the token is not returned
     * @return boolean successful authentication
     */
    public function authenticate($key, $secret)
    {
        $token = $this->factory->getServerAuth()->authenticate($key, $secret);

        $this->state->set('token', $token);
        return $token;
    }

    /**
     * Clones the selected template to create an assignable Workflow instance.
     * Any custom fields provided are persisted at this stage and may not be changed.
     *
     * @param  string $templateId      uuid4 template workflow ID as returned by the listTemplates call
     * @param  string $monitorableId   local ID for the monitorable resource. e.g. course ID or quiz ID.
     * @param  string $friendlyName    provide a friendly name that will appear for this in the reporting tools
     * @param  array  $customFields    Any merge fields defined by the template can be set here.
     *                                 They cannot be changed once the monitoring task has been started for any user.
     * @return string                  uuid4 the workflow ID
     */
    public function link($templateId, $monitorableId, $friendlyName, array $customFields = [])
    {
        $body = $this->factory->getWorkflow()->link($templateId, $monitorableId, $friendlyName, $customFields);
        //@todo check the response is OK
        return $monitorableId;
    }

    /**
     * List the available templates for monitoring, used for configuration
     *
     * @return array available templates to clone
     */
    public function templates()
    {
        return $this->factory->getWorkflow()->listTemplates();
    }

    /**
     * Provides a user access token and internal IDs for booting the Verify JavaScript SDK.
     *
     * Once this has been called against a `link`ed template,
     * that template cannot be modified but may be replaced by a new call to `link`
     *
     * @param  string $userId        your local user ID
     * @param  string $monitorableId your local monitorable ID
     * @return array                 relevant configuration to pass to the browser for beginning monitoring
     *
     * @see self::link for where to set a monitorable ID
     */
    public function begin($userId, $monitorableId)
    {
        return $this->factory->getWorkflow()->activate($userId, $monitorableId);
    }

    /**
     * Create a user account to run the verify instance under.
     *
     * Normally this will be per-user of your system.
     *     *
     * @param  string $userId        your local user ID
     * @param  string $firstName     The user's first name, used for reporting data
     * @param  string $lastName      The user's last name, used for reporting data
     * @param  string $email         The user's email address, used for reporting data
     * @return array                 successful user creation
     */
    public function createUser($userId, $firstName, $lastName, $email = null)
    {
        return $this->factory->getUser()->create($userId, $firstName, $lastName, $email);
    }

    /**
     * Upload reference photo
     *
     * Normally this will be per-user of your system, upload a file into comprobo to use as a reference image
     * for facial comparison
     *
     * @param  string $userId        your local user ID
     * @param  array $file           array matching the structure of $_FILES['your-field'] array.
     * @return array                 successful upload
     */
    public function setReferencePhoto($userId, array $file)
    {
        return $this->factory->getUser()->setReferencePhoto($userId, $file);
    }

    /**
     * Check for reference photo
     *
     * Check to see if a reference photo exists for the specified User
     *
     * @param  string $userId        your local user ID
     * @return boolean               flag to indicate whether photo exists
     */
    public function referencePhotoExists($userId)
    {
        return $this->factory->getUser()->referencePhotoExists($userId);
    }
}
