<?php
namespace Comprobo\Verify;

/**
 * In-memory application state, shared between internal classes
 *
 * @see  Factory
 */
class State
{
    private $state = [];

    /**
     * Set a value in the current execution context
     * @param string $key
     * @param mixed $value
     */
    public function set($key, $value)
    {
        if (!is_string($key)) {
            throw new Exceptions\Misconfiguration("State keys must be strings");
        }

        $this->state[$key] = $value;
    }

    /**
     * retrieve a value from the current execution context
     *
     * @param string $key
     * @return mixed the stored value or null if not found
     */
    public function get($key)
    {
        return $this->has($key) ? $this->state[$key] : null;
    }

    /**
     * determine whether a key is set
     * @param   string $key
     * @return  bool key exists
     */
    public function has($key)
    {
        return array_key_exists($key, $this->state);
    }
}
