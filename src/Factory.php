<?php
namespace Comprobo\Verify;

use InvalidArgumentException;

/**
 * Provides depedency resolution and injection for various classes in the library.
 * Normally not used directly by a third-party but can be if required for some reason.
 */
class Factory
{
    /**
     * @var array
     */
    private $config;
    /**
     * @var State
     */
    private $state;

    /**
     * Create a new Comprobo\Verify\Factory
     *
     * @param array $userConfig any custom configuration variables that need to be overwritten
     */
    public function __construct(array $userConfig = ['env' => 'production'])
    {
        $requiredKeys = ['env'];

        foreach ($requiredKeys as $key) {
            // @todo More validation on these items.
            if (!array_key_exists($key, $userConfig)) {
                throw new Exceptions\Misconfiguration("Missing required config key $key");
            }
        }

        $this->config = array_merge($userConfig, $this->moduleConfig($userConfig));
        $this->state = new State();
    }

    /**
     * @return Comprobo\Verify\Api The common API methods such as authenticating your server
     */
    public function getApi()
    {
        return new Api($this);
    }

    /**
     * @return Comprobo\Verify\Workflow workflow service integration
     */
    public function getWorkflow()
    {
        return new Workflow($this);
    }

    /**
     * @return Comprobo\Verify\User user service integration
     */
    public function getUser()
    {
        return new User($this);
    }

    /**
     * Treated as a singleton in the application
     *
     * @return Comprobo\Verify\State the current API state such as access token and organisation context
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     *  @return array the current SDK config
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @return Auth\Server the auth service integration
     */
    public function getServerAuth()
    {
        return new Auth\Server($this);
    }

    /**
     * @return HTTP\Request HTTP client wrapper for making system calls
     */
    public function getRequest()
    {
        return new HTTP\Request();
    }

    /**
     * @return string url for JavaScript files that must be included to operate the SDK in the browser
     */
    public function javascriptPath()
    {
        return $this->config['urls']['javascriptPath'];
    }

    /**
     * @return string url for css files that must be included to operate the SDK in the browser
     */
    public function cssPath()
    {
        return $this->config['urls']['cssPath'];
    }

    /**
     * @param  array $userConfig any custom parameters passed to the application by the caller
     * @return array environment-specific module configuration
     */
    private function moduleConfig(array $userConfig)
    {
        switch (strtolower($userConfig['env'])) {
            case 'production':
                $baseUrl = 'https://verify.comprobo.net/v1/';
                break;
            case 'staging':
                $baseUrl = 'https://staging-verify.comprobo.net/v1/';
                break;
            case 'test':
                $baseUrl = 'https://v2test-verify.comprobo.net/v1/';
                break;
            case 'local':
                $baseUrl = 'http://localhost:8084/v1/';
                break;
            default:
                throw new InvalidArgumentException("Invalid environment specified");
        }

        return [
            'urls' => [
                'auth'            => $baseUrl . 'authenticate',
                'javascriptPath'  => $baseUrl . 'verify.js',
                'cssPath'         => $baseUrl . 'verify.css',
                'templateList'    => $baseUrl . 'templates/list',
                'linkTemplate'    => $baseUrl . 'templates/{templateId}/link',
                'begin'           => $baseUrl . 'assignment/{userId}/{monitorable}',
                'userCreate'      => $baseUrl . 'user',
                'setRefPhoto'     => $baseUrl . 'user/{userId}/photo',
                'headRefPhoto'     => $baseUrl . 'user/{userId}/photo'
            ]
        ];
    }
}
