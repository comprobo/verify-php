# Comprobo Verify PHP SDK

## Installation

Requires PHP version >= 5.4

`composer require comprobo/verify-php`

## Basic Usage

```
require 'vendor/autoload.php';

$verify = new Comprobo\Verify\Api::create();
$verify->authenticate($myKey, $mySecret);

// ready to go!

```

Fuller examples can be found in `USAGE.md`

## Documentation

Full API documentation can be found in the `docs/` directory. To ensure you have the latest version, run `composer doc`

### Note

This API is under active development and internal workings may be refactored often. If you require a hidden or internal class or method exposed, open a pull request to do so via the `Comprobo\Verify\Api` class.

## Contributing

Pull requests are welcome, please adhere to PSR-2 coding standards using PSR-4 autoloading for classes.

The changeset must pass the `composer phpcs` task with no outstanding errors in order to be accepted.

## Licence

All rights reserved.

