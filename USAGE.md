# How to use the Verify SDK

## Installation

Install the Verify PHP sdk; the easiest method is using [composer](https://getcomposer.org).

`composer require comprobo/verify-php`

ensure you have included the composer autoload file in your application:

`require /path/to/vendor/autoload.php`

## Basic usage

The Verify SDK is used primarily via the `Comprobo\Verify\Api` class.

```
$verify = Comprobo\Verify\Api::create();
```

In order to successfully use the methods of the `Api` class you must authenticate using your given Verify credentials.

```
/**
 * Throws Comprobo\Verify\Exceptions\Auth if the credentials are invalid
 * returns boolean to indicate a successful authentication.
 */
$verify->authenticate($myKey, $mySecret);
```

Once this step is completed you may continue to implement the given methods. Full API documentation is provided in the `doc/` directory.

## API Methods

| Name(params) | Return type | Description |
|--------------|-------------|-------------|
| `javascriptPath()` | string | The full URL for the javascript asset in order to run Verify in the browser. You must load this on any page running the Verify html snippet. |
| `authenticate(string $key, string $secret)` | boolean | Submit your credentials in exchange for an access token. This enables the rest of the SDK. |
| `templates()` | array | List the available monitoring templates. These will be used to allow monitoring for your application. |
| `createUser(string $userId, string $firstName, string $lastName, string $email=null)` | array | List the available monitoring createUser. These will be used to allow monitoring for your application. Email is optional, since the Verify platform will not contact your users directly. |
| `link(string $templateId, string $monitorableId, string $friendlyName, array $customFields = [])` | array | Assign a template to the given monitorableID, with the given name and any custom field data that is required. |
| `begin(string $userId, string $monitorableId)` | array | Retrieve the necessary details to pass to the browser to initiate monitoring for this user and monitorable. |

## Browser template

For your front-end, the SDK requires that you enqueue the application JavaScript with the `javascriptPath()` method. It will return you a fully-qualified HTTPS URL so can be safely included on the page without generating warnings.

The JavaScript side of Verify is written in AngularJS and requires the following code to be displayed in the place you would like to run Verify. The data to populate is as returned by `begin(string $userId, string $monitorableId)` method.

n.b. This is a naive implementation.

```

<?php
    $details = $verify->begin('my-user-id', 'my-monitorable-id');
?>

<div class="comprobo comprobo-verify" ng-app="comprobo" ng-controller="RemoteController"></div>
<input type="hidden" name="verify-details" id="verify-details" value="<?= json_encode($details) ?>">

<script src="<?= $verify->javascriptPath() ?>" type="text/javascript"></script>
<script type="text/javascript">

    // if you require a test environment, you can set the `verifyEnv` property on window.
    window.verifyEnv = 'staging';

    var monitoringDetails = JSON.parse(document.getElementById('verify-details').value);
    var workflow = monitoringDetails.workflow;
    var user     = monitoringDetails.user;
    var token    = monitoringDetails.token;

    /**
     * normally wait for the 'comprobo-ready' event or inspect
     * the window object to wait for the 'comproboApi' property to be set.
     */
    window.comproboApi.begin(workflow, user, token).then(function() {
        // The assignment has begun, the '.comprobo-verify' block will now
        // display the appropriate monitoring interface.
    });
</script>

```

To start the monitoring process, you must use the JavaScript API.

## JavaScript API

Once the `javascriptPath` file has been loaded in the browser, it will find the `.comprobo-verify` element and start the application.

Available on the window will be the `comproboAPI` property.

Additionally you can wait for the `comprobo-ready` event and receive it in the event callback with the signature `function(e, comproboApi)`.

### Methods

| Name(params)                                | Return  | Description                                                                                                                                                                                                                                                               |
|---------------------------------------------|---------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `start(assignmentId, userId, accessToken)`  | Promise | Begin the monitoring task. This will prompt for webcam access, begin the given monitoring workflow and begin spawning attention and monitoring tasks (depending on configuration.) n.b. All three parameters to supply are provided in the PHP SDK via the `begin` method. Additional calls to `start` during an in-progress assignment will be ignored and the promise resolved instantly. |
| `stop()`                                    | Promise | End the currently-running assignment, you should call this whenever your current activity (session, etc) is complete. Monitoring sessions that are not stopped may be flagged and will automatically expire after a period of inactivity.                                 |
| `log(message)`                              | Promise | Add an event to the monitoring log. These are temporal logs that can be plotted as they occurred during the monitoring activity and are made available after the completion of the activity.                                                                              |
| `completeTask()`                            | Promise | *(advanced)* Complete the currently-running monitoring task. You may need to use this if you have additional tasks specified in your template that are not automatically completed by the Verify application.                                                             |
